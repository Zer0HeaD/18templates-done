

#include <iostream>
#include <string>
using namespace std;

template <class T>


class Stack
{
private:
    int size;
    T* arr = NULL;

public:
    
    Stack()
    {
        size = 0;
    }
    ~Stack()
    {
        delete[] arr;
    }
    void FillArray()
    {
        for (int i = 0; i < size; i++)
        {
            arr[i] = rand() & 10;
        }
    }
    void PrintArray()
    {
        for (int i = 0; i < size; i++)
        {
            cout << arr[i] << "\t";
        }
        cout << endl;
    }
    void Push(T value)
    {
        T* NewArray = new T[size + 1];
        for (int i = 0; i < size; i++)
        {
            NewArray[i] = arr[i];
        }
        NewArray[size] = value;
        size++;
        delete[]arr;
        arr = NewArray;
    }
    void pop()
    {
        size--;
    }
};


int main()
{
    Stack<int> IntStack;
    Stack<char> CharStack;
    Stack<string> StringStack;

    cout << "----------int-------------" << endl;

    IntStack.FillArray();

    

    IntStack.Push(10);
    IntStack.Push(90);
    IntStack.Push(3);
    IntStack.Push(10);
    IntStack.Push(3);
    IntStack.Push(1);

    IntStack.PrintArray();

    IntStack.pop();
    IntStack.pop();
    
    

    IntStack.PrintArray();

    cout << "----------char-------------" << endl;

    CharStack.FillArray();
    

    CharStack.Push('a');
    CharStack.Push('r');
    CharStack.Push('t');
    CharStack.Push('h');
    CharStack.Push('u');
    CharStack.Push('r');

    CharStack.PrintArray();

    CharStack.pop();
    CharStack.pop();
    CharStack.pop();

    CharStack.PrintArray();

    cout << "----------String-------------" << endl;

    StringStack.FillArray();

    StringStack.Push("Skin");
    StringStack.Push("is");
    StringStack.Push("healed,");
    StringStack.Push("but");
    StringStack.Push("you're");
    StringStack.Push("bleeding");
    StringStack.Push("inside");

    StringStack.PrintArray();

    StringStack.pop();
    StringStack.pop();
    StringStack.pop();
    StringStack.pop();

    StringStack.PrintArray();




}
